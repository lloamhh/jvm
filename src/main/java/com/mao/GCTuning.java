package com.mao;

public class GCTuning {

    public static void main(String[] args) {

        // 返回虚拟机试图使用的最大内存
        long max = Runtime.getRuntime().maxMemory();
        // 返回JVM的初始化总内存
        long total = Runtime.getRuntime().totalMemory();

        System.out.println("max=" + max + "字节\t" + (max/(double)1024/1024) + "MB");
        System.out.println("total=" + total + "字节\t" + (total/(double)1024/1024) + "MB");

        // 默认情况下： 分配的总内存是电脑内存的 1/4，而初始化的内存是电脑内存的： 1/64
    }

    /*遇到OOM时：
    * 1、 尝试扩大堆内存看结果
    * 2、分析内存，看一下哪个地方出现了问题（需要用专业工具）
    * */


}
