package com.mao;

public class DemoNative {
    public static void main(String[] args) {

        new Thread(()->{

        },"my thread name").start();
    }

    /*native : 凡是带了native 关键字的，说明Java的作用范围达不到了，会去调用底层c语言的库
    * 会进入本地方法栈
    * 调用本地方法本地接口 JNI
    * JNI的作用： 扩展Java的使用，融合不同的编程语言为Java所用，最初：C、C++。
    * Java诞生的时候C、C++ 横行，想要立足，必须要有调用C、C++的程序
    * 它在内存区域中专门开辟了一块标记区域： 本地方法栈 Native Method Stack ，登记 native 方法
    * 在最终执行的时候，加载本地方法库中的方法通过JNI
    *
    * Java程序驱动打印机，管理系统，掌握即可，在企业即应用中较为少见
    */
    private native void start0();
}
