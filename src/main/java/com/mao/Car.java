package com.mao;

public class Car {

    private int age;

    public static void main(String[] args) {

        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();

        System.out.println(car1.hashCode());
        System.out.println(car2.hashCode());
        System.out.println(car3.hashCode());

        Class<? extends Car> aClass = car1.getClass();

        System.out.println(aClass);

        ClassLoader classLoader = aClass.getClassLoader();

        System.out.println(classLoader);  //AppClassLoader

        System.out.println(classLoader.getParent()); // PlatformClassLoader   /jre/lib/ext

        System.out.println(classLoader.getParent().getParent());//null 1.不存在 2.Java程序获取不到  /rt.jar

        System.out.println("===========================");
        String s = new String();

        System.out.println(s.getClass().getClassLoader());

    }
}
