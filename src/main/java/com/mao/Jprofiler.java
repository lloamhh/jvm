package com.mao;

import java.util.ArrayList;
import java.util.List;

// -Xms1m -Xmx8m -XX:+HeapDumpOnOutOfMemoryError
/*
* -Xms 设置初始化内存分配大小 1/64
* -Xmx 设置最大分配内存，默认 1/4
* -XX:+PrintGCDetails 打印GC垃圾回收信息
* -XX:+HeapDumpOnOutOfMemory  发生OOM错误，Dump出文件
* */
public class Jprofiler {

    byte[] bytes = new byte[1*1024*1024]; // 1M

    public static void main(String[] args) {


        List<Jprofiler> list = new ArrayList<>();
        int count = 0;
        try {
            while (true){
                list.add(new Jprofiler());   // 问题所在
                count++;
            }
        } catch (OutOfMemoryError e) {
            System.out.println("count:"+count);
            e.printStackTrace();
        }

    }
}
