# JVM探究

- 请你谈谈你对JVM的理解？  
  - java-->class-JVM
  - java8虚拟机和之前的变化更新
- 什么是OOM，什么是栈溢出StackOverFlowError?怎么分析？
- JVM的常用调优参数有哪些？
- 内存快照如何抓取，怎么Dump文件？知道吗？
- 谈谈JVM中，类加载器你的认识？ rt-jar ext application



## 1、JVM的位置

![image-20210405115600875](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405115600875-1621564511837.png)

## 2、JVM的体系结构

![image-20210405121139213](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405121139213.png)

![image-20210405121404605](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405121404605.png)

![image-20210405121512667](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405121512667.png)

## 3、类加载器

作用：加载Class文件

![image-20210405145318680](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405145318680.png)

1. 虚拟机自带的加载器

2. 启动类（根）加载器

3. 扩展类加载器

4. 应用程序（系统类）加载器

5. 百度：双亲委派机制

6. 双亲委派机制

   - [Java类加载机制，你理解了吗](https://baijiahao.baidu.com/s?id=1636309817155065432&wfr=spider&for=pc)
   - [面试官：java双亲委派机制及作用](https://www.jianshu.com/p/1e4011617650)

   在介绍双亲委派机制的时候，不得不提ClassLoader（类加载器）。说ClassLoader之前，我们得先了解下Java的基本知识。  
   Java是运行在Java的虚拟机(JVM)中的，但是它是如何运行在JVM中了呢？我们在IDE中编写的Java源代码被编译器编译成.class的字节码文件。然后由我们得ClassLoader负责将这些class文件给加载到JVM中去执行。

   JVM中提供了三层的ClassLoader：

   - **Bootstrap classLoader**:主要负责加载核心的类库(java.lang.*等)，构造ExtClassLoader和APPClassLoader。

   - **ExtClassLoader**：主要负责加载jre/lib/ext目录下的一些扩展的jar。

   - **AppClassLoader**：主要负责加载应用程序的主函数类

     那如果有一个我们写的Hello.java编译成的**Hello.class**文件，它是如何被加载到JVM中的呢？别着急，请继续往下看。

   ### **双亲委派机制**

   ```java
   
       public Class<?> loadClass(String name) throws ClassNotFoundException {
           return loadClass(name, false);
       }
       //              -----??-----
       protected Class<?> loadClass(String name, boolean resolve)
           throws ClassNotFoundException
       {
               // 首先，检查是否已经被类加载器加载过
               Class<?> c = findLoadedClass(name);
               if (c == null) {
                   try {
                       // 存在父加载器，递归的交由父加载器
                       if (parent != null) {
                           c = parent.loadClass(name, false);
                       } else {
                           // 直到最上面的Bootstrap类加载器
                           c = findBootstrapClassOrNull(name);
                       }
                   } catch (ClassNotFoundException e) {
                       // ClassNotFoundException thrown if class not found
                       // from the non-null parent class loader
                   }
    
                   if (c == null) {
                       // If still not found, then invoke findClass in order
                       // to find the class.
                       c = findClass(name);
                   }
               }
               return c;
   ```

   ![image-20210405154056125](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405154056125.png)

   从上图中我们就更容易理解了，当以Hello.class这样的文件要被加载时。不考虑我们自定义类加载器，首先会在APPClassLoader中**检查是否加载过**，如果有那就无需再加载了。如果没有，那么会拿到**父加载器**，然后调用父加载器的loadClass方法。父类中**同理也会先检查自己是否已经加载过**，如果没有再往上。注意这个类似递归的过程直到到达**Bootstrap ClassLoader**之前，**都是在检查是否加载过**，并不会选择自己去加载。直到**BootstrapClassLoader**，已经没有父加载器了，这时候开始考虑自己是否能加载了，如果自己无法加载，会下沉到子加载器去加载，一直到最底层，如果没有任何加载器能加载，就会抛出ClassNotFoundException。

   **为什么要设计这种机制**

   这种设计有个好处是，如果有人想替换系统级别的类：String.java。篡改它的实现，在这种机制下这些系统的类已经被Bootstrap classLoader加载过了（为什么？因为当一个类需要加载的时候，最先去尝试加载的就是BootstrapClassLoader），所以其他类加载器并没有机会再去加载，从一定程度上防止了危险代码的植入。

   ![img](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/2020121722082798.png)

## 4、沙箱安全机制

- java安全模型核心
- 沙箱：限制程序的运行时环境
- 域Domain概念
- 将Java代码限定在虚拟机特点的运行范围内，严格限制代码对本地系统资源的访问，这样措施保证对代码的有效隔离，防止对本地系统的破幻

## 5、Native

```java
public class DemoNative {
    public static void main(String[] args) {

        new Thread(()->{

        },"my thread name").start();
    }

    /*native : 凡是带了native 关键字的，说明Java的作用范围达不到了，会去调用底层c语言的库
    native -> jni -> 本地方法接口 -> 本地方法库
    * 会进入本地方法栈
    * 调用本地方法本地接口 JNI
    * JNI的作用： 扩展Java的使用，融合不同的编程语言为Java所用，最初：C、C++。
    * Java诞生的时候C、C++ 横行，想要立足，必须要有调用C、C++的程序
    * 它在内存区域中专门开辟了一块标记区域： 本地方法栈 Native Method Stack ，登记 native 方法
    * 在最终执行的时候，加载本地方法库中的方法通过JNI
    *
    * Java程序驱动打印机，管理系统，掌握即可，在企业即应用中较为少见
    */
    private native void start0();
}
```



## 6、PC寄存器

程序计数器：Program Counter Register

​	每个线程都有一个程序计数器，是线程私有的，就是一个指针，指向方法区中的方法字节码（用来存储指向像一条指令的地址，也即将要执行的指令代码），在执行引擎读取下一条指令，是一个非常小的内存空间，几乎可以忽略不计

## 7、方法区

Method Area方法区

​	方法区是被所有线程共享，所有字段和方法字节码，以及一些特殊方法，如构造函数，接口代码也在此定义，简单说，所有定义的方法的信息都保存在该区域，此区域属于共享区间；

​	==**静态变量、常量、类信息（构造方法、接口定义）、运行时的常量池存在方法区中，但是实例变量存在堆内存中，和方法区无关**==

## 8、栈

- 栈：数据结构
- 程序 = 数据结构 + 算法：持续学习
- 程序 = 框架 + 业务逻辑：吃饭用的
- 栈：先进后出、后进先出：桶
- 队列：先进先出（FIFO：First Input First Output）

为什么main先执行，后结束？

- 栈：栈内存，主管程序的运行，生命周期和线程同步；

- 线程结束，栈内存也就释放了，对于栈来说，不存在垃圾回收问题

- 一旦线程结束，栈就Over

- 栈：8大基本类型 + 对象引用 + 实例的方法都在栈内存中分配

- 栈运行原理：栈帧 

  ![image-20210405181126980](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405181126980.png)

- 栈满了：StackOverFlowError

- 栈 + 堆 + 方法区：交互关系

- ![image-20210405174927098](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405174927098.png)



## 9、三种JVM

- Sun公司 HotSpot `Java HotSpot(TM) 64-Bit Server VM (build 13.0.2+8, mixed mode, sharing)`
- BEA `JRockit`
- IBM `J9VM`

## 10、堆

Heap,一个JVM只有一个堆内存，堆内存的大小是可以调节的。

类加载器读取了类文件后，一般会把什么东西放到堆中？类、方法、常量、变量~，保存我们所有引用类型的真实对象；

堆内存中还要细分为三个区域：

- 新生区（伊甸园区）  Young/New

- 养老区 old

- 永久区 Perm

  ![image-20210405205034368](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405205034368.png)

  GC垃圾回收，主要在伊甸园区和养老区

  假设内存满了，会报OOM错误，堆内存不够 `java.lang.OutOfMemoryError: Java heap space`

  在JDk8以后，永久存储区改了个名字（元空间）；

## 11、新生区、老年区

**新生区**

- 类：诞生和成长的地方，甚至死亡

- 伊甸园：所有的对象都是在伊甸园区new出来的

- 幸存者区（0区，1区）

  ![image-20210405221918605](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405221918605.png)

  真理：经过研究，99%的对象都是临时对象

## 12、永久区

**永久区:**

这个区域常驻内存。用来存放JDK自身携带的Class对象。Interface元数据，存储的是Java运行时的一些环境或类信息，这个区域不存在垃圾回收！关闭VM虚拟机就会释放这个区域的内存



一个启动类，加载了大量的第三方jar包。Tomcat部署了太对的应用，大量动态生成的反射类。不断地被加载，知道内存满，就会出现OOM。



- jdk1.6之前：永久代，常量池是在方法区；

- jdk1.7:永久代，但是慢慢的退化了，`去永久代`，常量池在堆中

- jdk1.8之后：无永久代，常量池在元空间（运行时常量池，.class常量池在元空间，字符串常量池依旧在堆中）

  ![image-20210405215348157](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405215348157.png)

  **元空间逻辑上存在，物理上不存在**

```java
public class GCTuning {

    public static void main(String[] args) {

        // 返回虚拟机试图使用的最大内存
        long max = Runtime.getRuntime().maxMemory();
        // 返回JVM的初始化总内存
        long total = Runtime.getRuntime().totalMemory();

        System.out.println("max=" + max + "字节\t" + (max/(double)1024/1024) + "MB");
        System.out.println("total=" + total + "字节\t" + (total/(double)1024/1024) + "MB");
// 默认情况下： 分配的总内存是电脑内存的 1/4，而初始化的内存是电脑内存的： 1/64
    }
}
```

![image-20210405222441763](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405222441763.png)

手动给JVM初始化内存赋值

```java
-Xms1024m -Xmx1024m -XX:+PrintGCDetails
```

![image-20210405223626401](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210405223626401.png)

从图中可以看出元空间虽然看上去有内存，但是我们垃圾优先已经占有了1024MB内存，所以元空间是不占用内存的，因此**元空间是逻辑上存在，物理上不存在**

## 13、堆内存调优



在一个项目中，突然出现OOM故障，name该如何排除，研究为什么出错？

- 能够看到代码第几行出错：内存快照分析工具，MAT，Jprofiler
- debug，一行行分析代码！

**MAT，Jprofiler作用**：

- 分析Dump内存文件，快速定位内存泄漏；
- 获得堆中的数据
- 获得大的对象
- 、、、、
- 虚拟机基本配置参数
  - -Xms 设置初始化内存分配大小 1/64
  - -Xmx 设置最大分配内存，默认 1/4
  - -XX:+PrintGCDetails 打印GC垃圾回收信息
  - -XX:+HeapDumpOnOutOfMemory  发生OOM错误，Dump出文件

```java
// -Xms1m -Xmx8m -XX:+HeapDumpOnOutOfMemoryError
/*
* -Xms 设置初始化内存分配大小 1/64
* -Xmx 设置最大分配内存，默认 1/4
* -XX:+PrintGCDetails 打印GC垃圾回收信息
* -XX:+HeapDumpOnOutOfMemory  发生OOM错误，Dump出文件
* */
public class Jprofiler {

    public static void main(String[] args) {

        Byte[] bytes = new Byte[1024*1024]; // 1M

        ArrayList<Jprofiler> list = new ArrayList<>();
        int count = 0;
        try {
            while (true){
                list.add(new Jprofiler());   // 问题所在
                count++;
            }
        } catch (OutOfMemoryError e) {
            System.out.println("count:"+count);
            e.printStackTrace();
        }

    }
}

```

  }
}

![image-20210406105154858](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406105154858.png)

![image-20210406105217817](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406105217817.png)

## 14、GC

**GC：垃圾回收**

![image-20210406105947010](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406105947010.png)

JVM在进行GC时，并不是对这三个区域统一回收，大部分时候，回收都是新生代

- 新生代
- 幸存区（from，to)
- 老年区

GC两种类：轻GC（Minor GC,普通的GC） ，重GC（Full GC，全局GC)



**GC题目**

- JVM的内存模型和分区，详细到每个区放什么？
- 堆里面的分区有哪些？Eden，FROM，to，老年区，说说他们的特点
- GC的算法有哪些？标记清除法，标记压缩，复制算法，引用计数器，怎么用的？
- 轻GC和中GC分别在什么时候发生？ 

## 15、常用算法

**引用计数法**

![image-20210406111344018](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406111344018.png)

**复制算法**

![image-20210406112744825](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406112744825.png)

![image-20210406113854273](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406113854273.png)

- 该算法将内存平均分成两部分，然后每次只使用其中的一部分，当这部分内存满的时候，将内存中所有存活的对象复制到另一个内存中，然后将之前的内存清空，只使用这部分内存，循环下去

- 幸存区01， from...to...， 0和1互相不断交换，进行gc进行复制算法
- 若一直没有死进入到养老区

- 好处：没有内存的碎片
- 坏处：浪费了内存空间：多了一半空间永远是空To。假设对象100%存活（极端情况），那么比如幸存from区全部复制到to区，成本太大
- 复制算法最佳使用场景：对象存活度较低的时候：新生区

**标记清除算法**

![](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406143508453.png)

- 优点：不需要额外的空间
- 缺点：两次扫描，严重浪费时间，会产生内存碎片

**标记压缩**

![](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406143151652.png)

**标记压缩清除**

**先清除几次**，等内存碎片很多时

![image-20210406143515615](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406143515615.png)

再压缩！！

![image-20210406143420125](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406143420125.png)

## 16、JMM

**JMM：Java Memory Model**

1. 什么是JMM？

   **JMM：Java Memory Model**：Java内存模型

2. 它是干嘛的？：官方，其他人的博客，对应的视频中了解

   作用：缓存一致性协议，用于定义数据读写的规则（遵守，找到这个规则）。

   JMM定义了线程工作内存和主内存之间的抽象关系：县城之间的共享变量存储在主内存（Main Memory）中，每个线程都有一个私有的本地内存（Local Memory）

   ![在这里插入图片描述](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/20200818164234102.png)

   ![image-20210406152717559](https://gitee.com/lloamhh/spring-img/raw/master/img/JVM/image-20210406152717559.png)

   解决共享对象可见性这个问题：volilate关键字，作用：（保证可见性，不保证原子性，解决指令重排）

3. 它该如何学习？

   JMM：抽象的概念，理论

   JMM对这八种指令的使用，制定了如下规则：

   - 不允许read和load、store和write操作之一单独出现。即使用了read必须load，使用了store必须write

   - 不允许线程丢弃他最近的assign操作，即工作变量的数据改变了之后，必须告知主存
   - 不允许一个线程将没有assign的数据从工作内存同步回主内存
   - 一个新的变量必须在主内存中诞生，不允许工作内存直接使用一个未被初始化的变量。就是怼变量实施use、store操作之前，必须经过assign和load操作
   - 一个变量同一时间只有一个线程能对其进行lock。多次lock后，必须执行相同次数的unlock才能解锁
   - 如果对一个变量进行lock操作，会清空所有工作内存中此变量的值，在执行引擎使用这个变量前，必须重新load或assign操作初始化变量的值
   - 如果一个变量没有被lock，就不能对其进行unlock操作。也不能unlock一个被其他线程锁住的变量
   - 对一个变量进行unlock操作之前，必须把此变量同步回主内存

   　　JMM对这八种操作规则和对[volatile的一些特殊规则](https://www.cnblogs.com/null-qige/p/8569131.html)就能确定哪里操作是线程安全，哪些操作是线程不安全的了。但是这些规则实在复杂，很难在实践中直接分析。所以一般我们也不会通过上述规则进行分析。更多的时候，使用java的happen-before规则来进行分析。

   

## 17、总结

**总结**

内存效率：复制算法 > 标记清除算法 > 标记压缩算法 （时间复杂度）

内存整齐度：复制算法 = 标记压缩算法  > 标记清除算法

内存利用率：标记压缩算法 = 标记清除算法 > 复制算法



**思考：**难道没有最优算法吗？

答案：没有，没有最好的算法，只有最合适的算法-------> GC：分代收集算法



**年轻代：**

- 存活率低
- 适合使用复制算法

**老年代：**

- 区域大：存活率高
- 适合使用标记清楚（内存碎片不是太多） + 标记压缩混合实现

一天时间学JVM，不现实，要深究，必须要下去花时间，和多看面试题，以及《深入理解JVM》，但是，我们可以掌握一个学习 JVM 的方法





16. OOM的种类和原因

- java.lang.OutOfMemoryError
- 内存泄漏（memory leak） 是指程序中一动态分配的堆内存由于某种原因程序未释放，造成系统内存的浪费，导致程序运行减慢甚至系统奔溃等严重后果
- 内存溢出（out of memory）是指程序申请内存时， 没有足够的内存供申请者使用，说白就是内存不够用，此时就会报错OOM,即所谓的内存溢出
- OOM种类和原因
  - java堆溢出 ---- 既然堆是存放实例对象的，那就无限创建实例对象
  - 虚拟机栈溢出 ----- 虚拟机栈描述的是java方法执行的内存模型， 每个方法在执行的时候都会创建一个栈帧用于存储局部变量表，操作数栈、动态链接、方法出口等信息，本地方法栈和虚拟栈的区别是，虚拟机栈为虚拟机运行java方法服务，而本地方法栈为虚拟机提供native方法服务；在单线程的操作中，无论是由于栈帧太大，还是虚拟栈空间太小，当占空间无法分配时，虚拟机抛出的都是StackOverflowError，而不会得到OutOfMemoryError异常，在多线程情况下，则会抛出OutOfMemoryError异常
  - 本地方法栈溢出！
  - 方法区和运行时常量池溢出 - java堆和方法区： java堆区主要存放对象实例和数组等，方法区保存类信息，静态变量等等，运行时常量也是方法区的一部分， 这两块区域是线程共享的区域，只会抛出OutOfMemeryError。
  - 本机内存直接溢出 -- NIO有关（New input/Output）,引入了一种基于通道与缓存区的I/O方式，可是native函数库直接分配堆外内存， 然后通过一个存储在java堆中的对象作为这块内存的应用进行操作。这样能在一些场景中显著提高性能，因为避免了再Java堆和Native堆中来回复制数据

1. 百度，
2. 思维导图，